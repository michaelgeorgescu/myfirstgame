//
//  Utilities.swift
//  MyFirstGame
//
//  Created by Michael Georgescu on 7/24/15.
//  Copyright (c) 2015 Michael. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

public class Utilities {
    
    public init() {
        
    }
    
    public func convertPanDistanceToCameraDistance(panDistance: Float)-> Float {
        var maxCameraDistance:Float = 630
        return panDistance / maxCameraDistance
        
    }
    
    public func generateRandomNumber(topBound: Float) -> Float {
        var val = arc4random_uniform(1000)
        var randVal = Float(val) / Float(1000)
        //NSLog("rand val: %f", randVal)
        //var value = randVal * topBound
        return (Float(randVal) * topBound) - topBound/2
    }
    
    public func radToDeg(value:Float) -> Float {
        return value * 57.2957795
    }
    
    public func rotateVector(vector: SCNVector3, rotMatrix: SCNMatrix4) -> SCNVector3 {
        var x = rotMatrix.m11 * vector.x + rotMatrix.m12 * vector.y + rotMatrix.m13 * vector.z
        var y = rotMatrix.m21 * vector.x + rotMatrix.m22 * vector.y + rotMatrix.m23 * vector.z
        var z = rotMatrix.m31 * vector.x + rotMatrix.m32 * vector.y + rotMatrix.m33 * vector.z
        var result = SCNVector3(x: x, y: y, z: z)
        return result
    }
    
}