//
//  SwipeGestureAnalyzer.swift
//  MyFirstGame
//
//  Created by Michael on 7/21/15.
//  Copyright (c) 2015 Michael. All rights reserved.
//

import Foundation
import UIKit
import SceneKit

//determines whether a swipe has just occured

public class SwipeGestureAnalyzer {

    var velocity: Float
    let thresholdVelocity:Float = 10
    let normalVector = CGPoint(x: 0, y: 1)
    var finalAnglePanned:Float = 0
    var swipedUnits:Float = 0
    var finalPositionAfter:Float = 0
    enum CurrentState {
        case GestureStarted
        case GestureNotActive
    }

    public init() {
        velocity = 0
    }

    public func calculateVelocity(xVelocity: Float, yVelocity: Float)->Float {
        //velocity is 2-norm of x and y component
        return sqrt(pow( xVelocity, 2) + pow(yVelocity, 2))
    }

    public func calculateCurrentPanDistance(startX: Float, startY: Float, currentX: Float, currentY: Float) -> Float {
        return sqrt(pow((currentX - startX), 2) + pow((currentY - startY), 2))
    }

    public func isAboveThresholdVelocityForJump(velocity: Float) -> Bool {
        return velocity > self.thresholdVelocity
    }
    
    public func calcFinalAnglePannedInRad() -> Float {
        return swipedUnits * 0.0012 //radians per unit
    }
    
    public func calcVectorOfSwipe(startPoint: CGPoint, endPoint: CGPoint) -> CGPoint {
        var newX = startPoint.x - endPoint.x
        var newY = startPoint.y - endPoint.y
        var v = CGPoint(x: newX, y: newY) //why do I need two labels instead of one?
        return v
    }
    
    func calc2Norm(x:CGFloat, y:CGFloat) -> CGFloat {
        return sqrt(pow(x, 2) + pow(y, 2))
    }
    
    func calcDotProduct(vector1: CGPoint, vector2: CGPoint) -> Float {
        return Float((vector1.x * vector2.x) + (vector1.y * vector2.y))
    }
    
    func normalize(vector:CGPoint) -> CGPoint {
        var result = CGPoint()
        var normOfVec = calc2Norm(vector.x, y:vector.y);
        result.x = vector.x / normOfVec
        result.y = vector.y / normOfVec
        return result
    }
    
    public func calcAngleOfSwipe(startPoint: CGPoint, endPoint: CGPoint) -> Float {
        var v = calcVectorOfSwipe(startPoint, endPoint: endPoint)
        var vNormalized = normalize(v)
        var cosTheta = calcDotProduct(normalVector, vector2: vNormalized)
        var angle = acos(cosTheta)
        return Float(angle)
    }
    
}