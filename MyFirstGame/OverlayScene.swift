//
//  OverlayScene.swift
//  MyFirstGame
//
//  Created by Michael Georgescu on 8/6/15.
//  Copyright (c) 2015 Michael. All rights reserved.
//

import Foundation

import UIKit
import SpriteKit

class OverlayScene: SKScene, GameViewControllerProtocol {
    
    var pauseNode: SKSpriteNode!
    var scoreNode: SKLabelNode!
    var centerPoint: SKLabelNode!
    var score:Float = 0
    
    override init(size: CGSize) {
        super.init(size: size)
        
        self.backgroundColor = UIColor.clearColor()
        
        let spriteSize = size.width/12
        //self.pauseNode = SKSpriteNode(imageNamed: "Pause Button")
        //self.pauseNode.size = CGSize(width: spriteSize, height: spriteSize)
        //self.pauseNode.position = CGPoint(x: spriteSize + 8, y: spriteSize + 8)
        
        self.scoreNode = SKLabelNode(text: "Score: \(score)")
        self.scoreNode.fontName = "Chalkdust"
        self.scoreNode.fontColor = UIColor.blackColor()
        self.scoreNode.fontSize = 24
        self.scoreNode.position = CGPoint(x: size.width/2, y: 20)
        
        self.centerPoint = SKLabelNode(text: " . ")
        self.centerPoint.fontName = "Chalkdust"
        self.centerPoint.fontColor = UIColor.yellowColor()
        self.centerPoint.fontSize = 40
        self.centerPoint.position = CGPoint(x: size.width/2, y: size.height/2)
        
        //self.addChild(self.pauseNode)
        self.addChild(self.scoreNode)
        self.addChild(self.centerPoint)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func onDistanceChanged(distance: Float) {
        scoreNode.text = "score: \(distance)"
    }
}
