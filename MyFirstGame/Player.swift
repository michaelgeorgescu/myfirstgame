//
//  File.swift
//  MyFirstGame
//
//  Created by Michael Georgescu on 8/13/15.
//  Copyright (c) 2015 Michael. All rights reserved.
//

import Foundation
import SceneKit

public class Player : SCNNode {

    let rotationIncrement:Float = 0.00024
    

    func setupNode() {
        
        //set up physics
        var boxForCamera = SCNBox(width: 3, height: 3, length: 7, chamferRadius: 0.01)
        var phyShape = SCNPhysicsShape(geometry: boxForCamera, options: nil)
        var physicsBody = SCNPhysicsBody(type: SCNPhysicsBodyType.Dynamic, shape:phyShape)
        physicsBody.mass = 1
        self.physicsBody = physicsBody
        
        //set up geometry
        geometry = boxForCamera
        
    }
    
    func jump() {
                
        //calculate globalForceDirection
        var localForceDirection = SCNVector3(x: 0, y: 40, z: -20)
        var tempPosition = position
        position = SCNVector3Zero //zero camera node to get global force direction
        var globalForceDirection = convertPosition(localForceDirection, toNode: parentNode)

        //zero velocity so direction of jump can be fine controlled
        physicsBody?.velocity = SCNVector3Zero
        
        //perform jump using physics
        physicsBody?.applyForce(globalForceDirection, atPosition: SCNVector3Zero, impulse: true)
        
        //set position back to existing value
        position = tempPosition

    }
    
    func rotate(unitsSwiped: Float) {
        
        var position = presentationNode().position
        var w = rotationIncrement * unitsSwiped
        //NSLog("rotation radians: \(w)")
        
        var rotation = SCNVector4(x: 0, y: 1, z: 0, w: rotationIncrement * unitsSwiped)
        
        //set rotation before position
        self.rotation = rotation
        self.position = position
    }
    
    func rotateAboutX(angle: Float) {
        var positionTemp = presentationNode().position
        position = SCNVector3Zero
        var rotation = SCNVector4(x: 1, y: 0, z: 0, w: angle)
        self.rotation = rotation
        position = positionTemp
    }


    
    
}