//
//  MyFirstGameTests.swift
//  MyFirstGameTests
//
//  Created by Michael on 7/19/15.
//  Copyright (c) 2015 Michael. All rights reserved.
//

import UIKit
import XCTest
import MyFirstGame
import SceneKit

class MyFirstGameTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        XCTAssert(true, "Pass")
    }
    
    func testCalculateVelocity() {
        var xVelocity: Float = 4.5
        var yVelocity: Float = 3.2
        var swipeGestureAnalyzer = SwipeGestureAnalyzer()
        var velocity = swipeGestureAnalyzer.calculateVelocity(xVelocity, yVelocity: yVelocity)
        NSLog("\(velocity)")
        XCTAssertEqual(5.52177507691, velocity, "")
    }
    
    func testCalculateCurrentPanDistance() {
        var xStart:Float = 1.0
        var yStart:Float = 1.5
        var xCurrent:Float = 4.5
        var yCurrent:Float = 5.3
        var swipeGestureAnalyzer = SwipeGestureAnalyzer()
        var result = swipeGestureAnalyzer.calculateCurrentPanDistance(xStart, startY: yStart, currentX: xCurrent, currentY: yCurrent)
        XCTAssertEqualWithAccuracy(5.16624, result, 0.0001, "")
    }
    
    func testConvertPanDistanceToCameraDistance() {
        var util = Utilities()
        var result = util.convertPanDistanceToCameraDistance(100)
        XCTAssertEqualWithAccuracy(0.15873, result, 0.0001, "")
    }
    
    func testIsAboveThresholdVelocityForJump() {
        var swipeGestureAnalyzer = SwipeGestureAnalyzer()
        var result = swipeGestureAnalyzer.isAboveThresholdVelocityForJump(11)
        XCTAssertEqual(true, result, "")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock() {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testGenerateRandomNumber() {
        let utilities = Utilities()
        var randNumber = utilities.generateRandomNumber(10)
        NSLog("value: %f", randNumber)
        
    }
    
    func testCalcVectorOfSwipe() {
        var swipeAnalyzer = SwipeGestureAnalyzer()
        var start = CGPoint(x: 3, y: 2)
        var end = CGPoint(x: 1, y: 7)
        var v = swipeAnalyzer.calcVectorOfSwipe(start, endPoint: end)
        NSLog("vector: x: %f y: %f", v.x, v.y)
    }
    
    func testCalcAngleOfSwipe() {
        var swipeAnalyzer = SwipeGestureAnalyzer()
        var util = Utilities()
        var startPoint = CGPoint(x: 3, y: 2)
        var endPoint = CGPoint(x: 1, y: 7)
        var angle = swipeAnalyzer.calcAngleOfSwipe(startPoint, endPoint: endPoint)
        NSLog("angle: %f", util.radToDeg(angle))
    }
    
    func testRotateVector() {
        var util = Utilities()
        var vector = SCNVector3(x: 1, y: 2, z: 3)
        var matr = SCNMatrix4()
        matr.m11 = 1
        matr.m12 = 2
        matr.m13 = 3
        matr.m21 = 4
        matr.m22 = 5
        matr.m23 = 6
        matr.m31 = 7
        matr.m32 = 8
        matr.m33 = 9
        var result = util.rotateVector(vector, rotMatrix: matr)
        NSLog("matrix result: x: %f y: %f z: %f", result.x, result.y, result.z)
        
    }
    
}
